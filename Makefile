CP=cp
RM=rm -f

all:
	"$(MAKE)" -C intro;
	"$(MAKE)" -C resident;
	"$(MAKE)" -C shaken;
	"$(MAKE)" -C cart clean;
	"$(MAKE)" -C cart;
	$(CP) cart/cart.lnx stirred.lnx

clean:
	"$(MAKE)" -C cart clean;
	"$(MAKE)" -C intro clean;
	"$(MAKE)" -C shaken clean;
	"$(MAKE)" -C resident clean;
	$(RM) stirred.lnx

