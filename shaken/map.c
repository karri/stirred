/*
 * Handle map view
 */
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <stdlib.h>
#include "../resident/tgi_setstdcolor.h"

enum mapmodes {
    MAP,
    SCAN,
    GO
} mapmodes;

extern void draw_enemies();
extern void drawmap(unsigned char x, unsigned char y);
extern int countdown(unsigned char middle);
extern void scanarea(unsigned char x, unsigned char y);
extern void draw_player();
extern void heal_player(unsigned char love);

int selx = 80;
int sely = 51;

void maploop() {
    int menuindex = 0;
    unsigned char keeprunning = 1;
    enum mapmodes mode = MAP;
    tgi_stdclear();
    while (keeprunning) {
        unsigned char joy;
        joy = joy_read(JOY_1);
        tgi_setstdcolor(BGCOLOR);
        tgi_bar(selx, 0, selx, 101);
        tgi_bar(0, sely, 159, sely);
        switch (mode) {
        case MAP:
            if (JOY_BTN_UP(joy)) {
                sely--;
            }
            if (JOY_BTN_DOWN(joy)) {
                sely++;
            }
            if (JOY_BTN_LEFT(joy)) {
                selx--;
            }
            if (JOY_BTN_RIGHT(joy)) {
                selx++;
            }
            if (JOY_BTN_FIRE(joy)) {
                scanarea(selx, sely);
                keeprunning = 0;
            }
            break;
        default:
            break;
        }
        tgi_setstdcolor(STD_LIGHTGREY);
        tgi_bar(selx, 0, selx, 101);
        tgi_bar(0, sely, 159, sely);
        drawmap(selx, sely);
        if (countdown(0)) keeprunning = 0;
        heal_player(0);
        draw_player();
    }
    while (joy_read(JOY_1))
        ;
}

