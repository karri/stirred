/*
 * clues
 */
#include <lynx.h>
#include <tgi.h>
#include <stdlib.h>

void clue(int index, unsigned char lying) {
    char *txt;

    switch (index ^ lying) {
    case 0:
        txt = "1.Cut red wire";
        break;
    case 1:
        txt = "1.Cut black wire";
        break;
    case 2:
        txt = "2.Flip yellow switch";
        break;
    case 3:
        txt = "2.Flip purple switch";
        break;
    case 4:
        txt = "3.Push white button";
        break;
    case 5:
        txt = "3.Push brown button";
        break;
    case 6:
        txt = "4.Push blue switch";
        break;
    case 7:
        txt = "4.Push green switch";
        break;
    }
    tgi_setcolor(6);
    tgi_bar(0, 87, 159, 97);
    tgi_setcolor(1);
    tgi_outtextxy(2, 89, txt);
}
