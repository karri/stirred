/*
 * The code and data defined here is placed in resident RAM
 */
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <stdlib.h>

extern unsigned char gas_left;
unsigned int __fastcall__ getscore(void);
int encounter_starts;

typedef struct ENEMY {
    unsigned char x;
    unsigned char y;
    int energy;
    unsigned char delay;
} ENEMY;

ENEMY enemies[8] = {
    {20, 80 - 60, 0, 0}, // Left+1
    {65, 30, 0, 0}, // Left+3
    {80, 35, 0, 0}, //Left+4
    {2, 101 - 60, 0, 0}, // Left
    {110, 40, 0, 0}, // Left+5
    {38, 101 - 60, 0, 0}, // Left+2
    {130, 30, 0, 0}, // Right
    {73, 33, 0, 0} // Left+3.5
};

ENEMY girl = {
    159-26-20, 101-50, 0, 0
};

unsigned char girls_alive[8] = {0, 0, 1, 1, 1, 1, 1, 0};

extern unsigned char lastIndex;
extern unsigned char lastSquare;
extern unsigned char lastSquareBase;

static SCB_REHV_PAL characterSprite = {
    BPP_1 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    100, 52,
    0x100, 0x100,
    {0x70,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

void draw_enemies(void)
{
    int i;
    for (i = 0; i < 8; i++) {
        if (enemies[i].energy && (enemies[i].delay == 0)) {
            characterSprite.hpos = enemies[i].x;
            characterSprite.vpos = enemies[i].y;
            characterSprite.data = 0;
            tgi_sprite(&characterSprite);
            tgi_setcolor(7);
            tgi_bar(enemies[i].x, enemies[i].y-1, enemies[i].x+20, enemies[i].y-2);
            tgi_setcolor(15);
            tgi_bar(enemies[i].x, enemies[i].y-1, enemies[i].x+enemies[i].energy/25, enemies[i].y-2);
        }
    }
}

void draw_girl(void)
{
    if (girl.energy && (girl.delay == 0)) {
        characterSprite.hpos = girl.x;
        characterSprite.vpos = girl.y;
        characterSprite.data = 0;
        tgi_sprite(&characterSprite);
        tgi_setcolor(7);
        tgi_bar(girl.x, girl.y-1, girl.x+20, girl.y-2);
        tgi_setcolor(15);
        tgi_bar(girl.x, girl.y-1, girl.x+girl.energy/25, girl.y-2);
    }
}

extern int gun_hit;
extern int gas_hit;

void enemy_shoot(int x, int y) {
    int i;
    for (i = 0; i < 8; i++) {
        if (enemies[i].energy && (enemies[i].delay == 0)) {
            if ((x > enemies[i].x) && (x < enemies[i].x + 26) &&
                (y > enemies[i].y) && (y < enemies[i].y + 60)) {
                enemies[i].energy -= gun_hit;
                if (enemies[i].energy < 0) enemies[i].energy = 0;
            }
        }
    }
    if (girl.energy && (girl.delay == 0)) {
        if ((x > girl.x) && (x < girl.x + 26) &&
            (y > girl.y) && (y < girl.y + 60)) {
            girl.energy -= gun_hit;
            if (girl.energy <= 0) {
                girl.energy = 0;
                girls_alive[lastSquareBase] = 0;
            }
        }
    }
}

unsigned char grenade_active = 0;

void throw_grenade() {
    if (gas_left) {
        grenade_active = 100;
        gas_left--;
    }
}

void enemy_grenade() {
    int i;
    for (i = 0; i < 8; i++) {
        if (enemies[i].energy && (enemies[i].delay == 0)) {
            enemies[i].energy -= gas_hit;
            if (enemies[i].energy < 0) enemies[i].energy = 0;
        }
    }
    if (girl.energy && (girl.delay == 0)) {
        girl.energy -= gas_hit;
        if (girl.energy <= 0) {
            girl.energy = 0;
            girls_alive[lastSquareBase] = 0;
        }
    }
}

void dot() {
    if (grenade_active) {
        grenade_active--;
        enemy_grenade();
    }
}

unsigned char enemies_left() {
    int i;
    unsigned char ret = 0;
    for (i = 0; i < 8; i++) {
        if (enemies[i].energy) {
            ret++;
        }
    }
    return ret;
}

unsigned char enemies_visible() {
    int i;
    unsigned char ret = 0;
    for (i = 0; i < 8; i++) {
        if (enemies[i].energy && (enemies[i].delay == 0)) {
            ret++;
        }
    }
    return ret;
}

unsigned char girls_left() {
    if (girl.energy) {
        return 1;
    }
    return 0;
}

unsigned char girls_visible() {
    if (girl.energy && (girl.delay == 0)) {
        return 1;
    }
    return 0;
}

extern unsigned char areacontent();

unsigned char nrof_enemies() {
    unsigned char enemies;
    if (areacontent() == 0) {
        return 0;
    }
    if (areacontent() == 6) {
        return 0;
    }
    if (areacontent() == 1) {
        srand(lastIndex);
        do {
            enemies = (rand() & 7) + 1;
        } while (enemies > 5);
        return enemies;
    }
    if (lastSquare & 0x40) {
        srand(lastIndex);
        do {
            enemies = (rand() & 7) + 2;
        } while (enemies > 8);
        return enemies;
    }
    return 0;
}


void create_girl() {
    if (girls_alive[lastSquareBase]) {
#ifdef TESTING
        girl.energy = 2;
#else
        girl.energy = 200;
#endif
    } else {
        girl.energy = 0;
    }
}

void heal_girl() {
    girl.energy = 400;
}

void create_enemies() {
    int i;
    for (i = 0; i < 8; i++) {
        enemies[i].energy = 0;
    }
    if ((lastSquareBase == 0) || ((lastSquareBase) == 6)) {
        return;
    }
    for (i = 0; i < nrof_enemies(); i++) {
#ifdef TESTING
        enemies[i].energy = 4;
#else
        enemies[i].energy = 400;
#endif
    }
}

void delay_enemies(unsigned char i, unsigned char d) {
    enemies[i].delay = d;
    encounter_starts = getscore();
}

void delay_girl(unsigned char d) {
    girl.delay = d;
    encounter_starts = getscore();
}

void delays(void) {
    if (encounter_starts != getscore()) {
        int i;
        for (i = 0; i < 8; i++) {
            if (enemies[i].delay)
                enemies[i].delay -= 1;
        }
        if (girl.delay)
            girl.delay -= 1;
        encounter_starts = getscore();
    }
}

