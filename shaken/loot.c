/*
 * loot
 */
#include <lynx.h>
#include <tgi.h>
#include <stdlib.h>

extern unsigned char lastSquareBase;
extern int player_maxenergy;
extern unsigned char lyinggirl;
extern unsigned char disarmsequence[4];
extern void clue(int index, unsigned char lying);
extern unsigned char gas_left;
extern unsigned char nightsight;
unsigned int __fastcall__ getscore(void);
extern unsigned char girls_visible();
extern unsigned char enemies_left();
extern void heal_martini();
extern unsigned char night();

unsigned char loot() {
    int now;
    char *txt;
    int isthereloot = 0;

    if ((lastSquareBase == 1) && (enemies_left() == 0)) isthereloot = 1;
    if (girls_visible()) isthereloot = 1;
    if (!isthereloot) return 0;

    switch (lastSquareBase) {
    default:
        break;
    case 2:
        txt = "Sandra says:";
        break;
    case 3:
        txt = "Inga says:";
        break;
    case 4:
        txt = "Lola says:";
        break;
    case 5:
        txt = "Miki says:";
        break;
    case 6:
        txt = "Penny says:";
        break;
    }
    if (lastSquareBase > 1) {
        tgi_setcolor(6);
        tgi_bar(0, 77, 159, 86);
        tgi_setcolor(1);
        tgi_outtextxy(2, 79, txt);
    }
    player_maxenergy += player_maxenergy / 8;
    tgi_setcolor(6);
    tgi_bar(0, 87, 159, 97);
    tgi_setcolor(1);
    if (lastSquareBase == 6) {
        switch (lyinggirl - 2) {
        case 0:
            tgi_outtextxy(2, 89, "Sandra is lying");
            break;
        case 1:
            tgi_outtextxy(2, 89, "Inga is lying");
            break;
        case 2:
            tgi_outtextxy(2, 89, "Lola is lying");
            break;
        case 3:
            tgi_outtextxy(2, 89, "Miki is lying");
            break;
        default:
            break;
        }
    } else {
        switch (lastSquareBase) {
        case 1:
            if ((rand() & 8)) {
                if ((rand() & 6) && (nightsight == 0) && night()) {
                    tgi_outtextxy(2, 89, "Got nightsight");
                    nightsight++;
                } else {
                    tgi_outtextxy(2, 89, "A martini sir");
                    heal_martini();
                }
            } else {
                tgi_outtextxy(2, 89, "Pick up TNT");
                gas_left++;
            }
            break;
        case 2:
            clue(disarmsequence[0], lyinggirl == 2);
            break;
        case 3:
            clue(disarmsequence[1], lyinggirl == 3);
            break;
        case 4:
            clue(disarmsequence[2], lyinggirl == 4);
            break;
        case 5:
            clue(disarmsequence[3], lyinggirl == 5);
            break;
        default:
            break;
        }
    }
    return 1;
}

