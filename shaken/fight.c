/*
 * Handle fighting main loop
 */
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include "../resident/tgi_setstdcolor.h"

enum fightmodes {
    MENU,
    GUN,
    LOVE,
    DISARM
};

extern unsigned char lastSquareBase;
extern unsigned char enemies_left();
extern unsigned char girls_left();
extern unsigned char enemies_visible();
extern unsigned char girls_visible();
extern void delays();
extern int player_energy;
extern int gas_left;
extern unsigned char loot();
extern unsigned char grenade_active;

static SCB_REHV_PAL bgSprite = {
    BPP_3 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    0, 0,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL fgSprite = {
    BPP_3 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    0, 0,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL sightSprite = {
    BPP_1 | TYPE_XOR,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    80, 51,
    0x100, 0x100,
    {0x08,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL smokeSprite = {
    BPP_1 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    80, 51,
    0x100, 0x100,
    {0x07,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

extern void enemy_shoot(int x, int y);
extern void draw_enemies();
extern void draw_girl();
extern void setdaypal();
extern void setdaylovepal();
extern void setdayhipal();
extern int countdown(unsigned char middle);
extern void leaveArea();
extern unsigned char bomblocation;
extern void draw_player();
extern void shoot_player();
extern void dot();
extern void throw_grenade();
extern void heal_player(unsigned char love);

void load_backgrounds() {
}

void drawbg0() {
}

void drawbg1() {
}

unsigned char fightloop() {
    enum fightmodes menumode = GUN;
    int menuindex = 0;
    unsigned char keeprunning = 1;
    unsigned char ret;
    unsigned char showloot;
    unsigned char usedoublebuffer = 2;
    unsigned char shootingeffect = 0;
    unsigned char shootingtimer = 0;
    unsigned char daypal;
    int hearttimer = 0;
    setdaypal();
    daypal = 1;
    tgi_stdclear();
    while (keeprunning) {
        unsigned char joy;
        if (enemies_visible()) {
            shootingeffect = 1;
            usedoublebuffer = 2;
            if (menumode == LOVE) {
                menumode = GUN;
                setdaypal();
            }
        } else {
            shootingeffect = 0;
        }
        if (enemies_left()) {
            showloot = 0;
        } else {
            showloot = 1;
        }
        joy = joy_read(JOY_1);
        switch (menumode) {
        case MENU:
            if (JOY_BTN_UP(joy)) {
                menuindex -= 1;
            }
            if (JOY_BTN_DOWN(joy)) {
                menuindex += 1;
            }
            if (JOY_BTN_FIRE(joy)) {
                if ((menuindex > 0) && (menuindex < 8)) {
                    menumode = GUN;
                }
                if ((menuindex > 10) && (menuindex < 18)) {
                    if (gas_left && (grenade_active == 0)) {
                        throw_grenade();
                        menumode = GUN;
                    }
                }
                if ((menuindex > 20) && (menuindex < 28)) {
                    if (girls_visible() && (shootingeffect == 0)) {
                        menumode = LOVE;
                        setdaylovepal();
                    }
                }
                if ((menuindex > 30) && (menuindex < 38)) {
                    if (showloot) {
                        keeprunning = 0;
                        ret = 0;
                    }
                }
                if ((menuindex > 40) && (menuindex < 48)) {
                    if (bomblocation == lastSquareBase) {
                        menumode = DISARM;
                        keeprunning = 0;
                        ret = 2;
                    }
                }
            }
            break;
        case GUN:
            if (JOY_BTN_UP(joy)) {
                if (sightSprite.vpos > 0) sightSprite.vpos -= 1;
            }
            if (JOY_BTN_DOWN(joy)) {
                if (sightSprite.vpos < 100) sightSprite.vpos += 1;
            }
            if (JOY_BTN_LEFT(joy)) {
                if (sightSprite.hpos > 0) sightSprite.hpos -= 1;
            }
            if (JOY_BTN_RIGHT(joy)) {
                if (sightSprite.hpos < 159) sightSprite.hpos += 1;
            }
            if (JOY_BTN_FIRE(joy)) {
                if (!lastSquareBase) {
                    keeprunning = 0;
                    ret = 0;
                } else {
                    enemy_shoot(sightSprite.hpos, sightSprite.vpos);
                    shootingeffect = 1;
                }
            }
            if (JOY_BTN_FIRE2(joy)) {
                if (!lastSquareBase) {
                    keeprunning = 0;
                    ret = 0;
                } else {
                    if (shootingeffect) {
                        if ((grenade_active == 0) && gas_left) {
                            throw_grenade();
                        }
                    } else {
                        menumode = MENU;
                        menuindex = 34;
                    }
                }
            }
            break;
        case LOVE:
            if (JOY_BTN_FIRE2(joy)) {
                usedoublebuffer = 2;
                if (enemies_left()) {
                    menumode = GUN;
                } else {
                    menumode = MENU;
                    menuindex = 34;
                }
                setdaypal();
            }
            break;
        default:
            break;
        }
        shootingtimer++;
        if (shootingtimer == 5) {
            if (shootingeffect && daypal) {
                setdayhipal();
                daypal = 0;
           } 
        }
        if (shootingtimer == 7) {
           if (!daypal) {
               setdaypal();
               daypal = 1;
           }
           shootingtimer = 0;
        }
        if (usedoublebuffer) {
            draw_enemies();
            draw_girl();
            if (grenade_active) {
                unsigned char i;
                smokeSprite.data = 0;
                smokeSprite.penpal[0] = 7;
                for (i = 0; i < 16; i++) {
                    smokeSprite.hpos = (rand() & 127);
                    smokeSprite.vpos = (rand() & 63) + 60;
                    tgi_sprite(&smokeSprite);
                }
            }
        }
        switch (menumode) {
        case MENU:
            if (tgi_getcolorcount() > 2) {
                tgi_setstdcolor(STD_DARKGREY);
                tgi_bar(0, 0, 44, 101);
            }
            tgi_setstdcolor(STD_GREY);
            tgi_outtextxy(2, 26 + menuindex, ">");
            tgi_outtextxy(10, 30, "Gun");
            if (gas_left) {
                tgi_outtextxy(10, 40, "TNT");
            }
            if (girls_visible() && (shootingeffect == 0)) {
                tgi_outtextxy(10, 50, "Love");
            }
            if (showloot) {
                tgi_outtextxy(10, 60, "Loot");
            }
            if (lastSquareBase == bomblocation) {
                tgi_outtextxy(10, 70, "Bomb");
            }
            break;
        case GUN:
            tgi_sprite(&sightSprite);
            break;
        case LOVE:
            {
                int x;
                int y;
                hearttimer++;
                if (hearttimer > 20) {
                    smokeSprite.data = 0;
                    smokeSprite.penpal[0] = (rand() & 1) ? 15 : 7;
                    x = rand() % 160;
                    y = rand() % 102;
                    smokeSprite.hpos = x;
                    smokeSprite.vpos = y;
                    tgi_sprite(&smokeSprite);
                    hearttimer = 0;
                }
                tgi_setviewpage(0);
                tgi_setdrawpage(0);
                if (usedoublebuffer) usedoublebuffer--;
            }
            break;
        default:
            break;
        }
        if (countdown(0)) {
            keeprunning = 0;
            ret = 3;
        }
        heal_player(menumode == LOVE);
        shoot_player();
        dot();
        draw_player();
        if (!player_energy) {
            keeprunning = 0;
            ret = 1;
        }
        delays();
    }
    while (joy_read(JOY_1))
        ;
    if (ret == 0) {
        if (loot()) {
            while (!joy_read(JOY_1))
                ;
            while (!joy_read(JOY_1))
                ;
        }
    }
    leaveArea();
    return ret;
}

