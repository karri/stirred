/*
 * Handle fighting main loop
 */
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <stdlib.h>
#include "../resident/tgi_setstdcolor.h"

static unsigned char sequence[4] = { 0, 0, 0, 0 };
extern unsigned char disarmsequence[4];
unsigned int __fastcall__ getscore(void);

static SCB_REHV_PAL bgSprite = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    0, 0,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL okSprite = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    80, 51,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL cursorSprite = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    0, 0,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

extern void setdefpal();
extern int countdown(unsigned char middle);

static unsigned char inside(unsigned char x, unsigned char y) {
    if (cursorSprite.hpos + 10 < x)
        return 0;
    if (cursorSprite.hpos + 10 > x + 20)
        return 0;
    if (cursorSprite.vpos + 10 < y)
        return 0;
    if (cursorSprite.vpos + 10 > y + 20)
        return 0;
    return 1;
}
static void box(unsigned char x, unsigned char y) {
    tgi_setcolor(7);
    tgi_bar(x + 5, y + 5, x+10, y+10);
}

static unsigned char checkdisarm(unsigned char index, unsigned char x1,
     unsigned char x2, unsigned char y)
{
    unsigned char i;
    if (sequence[index]) return 0;
    if (inside(x1, y)) {
        if (disarmsequence[index] == index*2) {
            for (i = 0; i < index; i++) {
                if (sequence[i] == 0) return 1;
            }
            sequence[index] = 1;
        } else {
            return 1; // oops
        }
    }
    if (inside(x2, y)) {
        if (disarmsequence[index] == index*2 + 1) {
            for (i = 0; i < index; i++) {
                if (sequence[i] == 0) return 1;
            }
            sequence[index] = 1;
        } else {
            return 1; // oops
        }
    }
    return 0;
}

int disarmloop() {
    int menuindex = 0;
    unsigned int now;
    unsigned char keeprunning = 1;
    unsigned char bigbadaboom = 0;
    setdefpal();
    while (keeprunning) {
        unsigned char joy;
        while (tgi_busy())
            ;
        joy = joy_read(JOY_1);
        if (JOY_BTN_UP(joy)) {
            if (cursorSprite.vpos > 0) cursorSprite.vpos -= 1;
        }
        if (JOY_BTN_DOWN(joy)) {
            if (cursorSprite.vpos < 100) cursorSprite.vpos += 1;
        }
        if (JOY_BTN_LEFT(joy)) {
            if (cursorSprite.hpos > 0) cursorSprite.hpos -= 1;
        }
        if (JOY_BTN_RIGHT(joy)) {
            if (cursorSprite.hpos < 159) cursorSprite.hpos += 1;
        }
        if (JOY_BTN_FIRE(joy)) {
            bigbadaboom += checkdisarm(0, 10, 140, 2);
            bigbadaboom += checkdisarm(1, 35, 110, 65);
            bigbadaboom += checkdisarm(2, 30, 115, 38);
            bigbadaboom += checkdisarm(3, 48, 100, 18);
            if (bigbadaboom) keeprunning = 0;
        }
        tgi_stdclear();
        tgi_sprite(&bgSprite);
        if (countdown(1)) {
            keeprunning = 0;
            bigbadaboom = 1;
        }
        if (sequence[0]) {
            okSprite.vpos = 2;
            if (disarmsequence[0] == 0) {
                okSprite.hpos = 10;
                tgi_sprite(&okSprite);
            }
            if (disarmsequence[0] == 1) {
                okSprite.hpos = 140;
                tgi_sprite(&okSprite);
            }
            if (sequence[1]) {
                okSprite.vpos = 65;
                if (disarmsequence[1] == 2) {
                    okSprite.hpos = 35;
                    tgi_sprite(&okSprite);
                }
                if (disarmsequence[1] == 3) {
                    okSprite.hpos = 110;
                    tgi_sprite(&okSprite);
                }
                if (sequence[2]) {
                    okSprite.vpos = 38;
                    if (disarmsequence[2] == 4) {
                        okSprite.hpos = 30;
                        tgi_sprite(&okSprite);
                    }
                    if (disarmsequence[2] == 5) {
                        okSprite.hpos = 115;
                        tgi_sprite(&okSprite);
                    }
                    if (sequence[3]) {
                        okSprite.vpos = 18;
                        if (disarmsequence[3] == 6) {
                            okSprite.hpos = 48;
                            tgi_sprite(&okSprite);
                            keeprunning = 0;
                        }
                        if (disarmsequence[3] == 7) {
                            okSprite.hpos = 100;
                            tgi_sprite(&okSprite);
                            keeprunning = 0;
                        }
                    }
                }
            }
        }
        tgi_sprite(&cursorSprite);
        tgi_updatedisplay();
    }
    now = getscore();
    do {
    } while (getscore() < now + 3);
    return bigbadaboom;
}

