/*
 * Create the world and the missions
 */
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <stdlib.h>
#include "../resident/tgi_setstdcolor.h"

#define ENEMYSQUARES 17
#define GOODGIRLSQUARES 1
#define BADGIRLSQUARES 4

#define off 0

unsigned char worldmap[64];
unsigned char lyinggirl;
unsigned char bomblocation;
unsigned char disarmsequence[4];

#ifdef TESTING
#define HIDE 0
#else
#define HIDE 0x80
#endif
#define AMBUSH 0x40
enum people {
    NONE = 0,
    ENEMY = 1,
    SANDRA = 2,
    INGA = 3,
    LOLA = 4,
    MIKI = 5,
    PENNY = 6
} people;

void create_world() {
    int i;
    int n;

    for (i = 0; i < 64; i++) worldmap[i] = NONE | HIDE;
    for (i = 0; i < ENEMYSQUARES; i++) {
        do {
            n = rand() & 63;
        } while ((worldmap[n] & 0x7F) != 0);
        worldmap[n] = ENEMY | HIDE;
    }
    do {
        n = rand() & 63;
    } while ((worldmap[n] & 0x7F) != 0);
    worldmap[n] = SANDRA | HIDE | AMBUSH;
    do {
        n = rand() & 63;
    } while ((worldmap[n] & 0x7F) != 0);
    worldmap[n] = INGA | HIDE | AMBUSH;
    do {
        n = rand() & 63;
    } while ((worldmap[n] & 0x7F) != 0);
    worldmap[n] = LOLA | HIDE | AMBUSH;
    do {
        n = rand() & 63;
    } while ((worldmap[n] & 0x7F) != 0);
    worldmap[n] = MIKI | HIDE | AMBUSH;
    do {
        n = rand() & 63;
    } while ((worldmap[n] & 0x7F) != 0);
    worldmap[n] = PENNY | HIDE;
    lyinggirl = (rand() % BADGIRLSQUARES) + SANDRA;
    bomblocation = (rand() % BADGIRLSQUARES) + SANDRA;
#ifdef TESTING
    n = 0;
#else
    n = rand();
#endif
    if (n & 0x0001) {
        disarmsequence[0] = 1;
    } else { 
        disarmsequence[0] = 0;
    }
    if (n & 0x0002) {
        disarmsequence[1] = 3;
    } else { 
        disarmsequence[1] = 2;
    }
    if (n & 0x0004) {
        disarmsequence[2] = 5;
    } else { 
        disarmsequence[2] = 4;
    }
    if (n & 0x0008) {
        disarmsequence[3] = 7;
    } else { 
        disarmsequence[3] = 6;
    }
}

unsigned char lastSquare;
unsigned char lastSquareBase;
unsigned char lastIndex;

void drawmap(unsigned char selx, unsigned char sely) {
    int i;
    int x;
    int y;
    tgi_setstdcolor(STD_GREY);
    tgi_outtextxy(46, 2 + off, "World Map");
    for (i = 0; i < 64; i++) {
        x = (i % 8) * 10 + 50;
        y = (i / 8) * 10 + 12 + off;
        switch (worldmap[i]) {
        case 0:
            tgi_setstdcolor(STD_LIGHTGREY);
            tgi_bar(x+1, y+1, x+3, y+3);
            break;
        case 1:
            tgi_setstdcolor(STD_RED);
            tgi_bar(x, y, x+4, y+4);
            break;
        case 2:
        case 2 | AMBUSH:
        case 3:
        case 3 | AMBUSH:
        case 4:
        case 4 | AMBUSH:
        case 5:
        case 5 | AMBUSH:
        case 6:
            tgi_setstdcolor(STD_LIGHTGREY);
            tgi_bar(x, y, x+4, y+4);
            break;
        default:
            tgi_setstdcolor(STD_LIGHTGREY);
            tgi_bar(x+2, y+2, x+2, y+2);
            break;
        }
        if ((x-3 < selx) && (x+7 > selx) && (y-3 < sely) && (y+7 > sely)) {
            lastIndex = i;
            lastSquare = worldmap[i] & 0x7f;
            lastSquareBase = lastSquare & 7;
            if ((worldmap[i] & 0x80) != 0x80) {
                tgi_setstdcolor(BGCOLOR);
                tgi_bar(44, 92, 159, 101);
                tgi_setstdcolor(lastSquareBase == 1 ? STD_RED : STD_GREY);
                switch (lastSquareBase) {
                case NONE:
                    tgi_outtextxy(44, 92 + off, "Safe zone");
                    break;
                case ENEMY:
                    tgi_outtextxy(44, 92 + off, "Armed troops");
                    break;
                case SANDRA:
                    tgi_outtextxy(44, 92 + off, "Casino");
                    break;
                case INGA:
                    tgi_outtextxy(44, 92 + off, "Spa");
                    break;
                case LOLA:
                    tgi_outtextxy(44, 92 + off, "Warehouse");
                    break;
                case MIKI:
                    tgi_outtextxy(44, 92 + off, "Garage");
                    break;
                case PENNY:
                    tgi_outtextxy(44, 92 + off, "Penny");
                    break;
                default:
                    break;
                }
            }
        }
    }
}

extern void create_enemies();
extern void create_girl();

void enterArea() {
    if ((worldmap[lastIndex] & AMBUSH) ||
        ((worldmap[lastIndex] & 0x7F) == ENEMY)) {
        create_enemies();
        worldmap[lastIndex] = worldmap[lastIndex] & 0x3f;
    }
    create_girl();
}

extern unsigned char enemies_left();

void leaveArea() {
    if (enemies_left())
        return;
    if (worldmap[lastIndex] == ENEMY)
        worldmap[lastIndex] = NONE;
}

void scanarea(unsigned char selx, unsigned char sely) {
    int i;
    int x;
    int y;
    for (i = 0; i < 64; i++) {
        x = (i % 8) * 10 + 50;
        y = (i / 8) * 10 + 12;
        if ((x-3 < selx) && (x+7 > selx) && (y-3 < sely) && (y+7 > sely)) {
            worldmap[i] = worldmap[i] & 0x7f;
        }
    }
}

unsigned char areacontent() {
    return worldmap[lastIndex];
}

