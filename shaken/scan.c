/*
 * Run the scan of a city block
 */
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <stdlib.h>
#include "../resident/tgi_setstdcolor.h"

#define off 0

extern unsigned char lastIndex;
extern unsigned char lastSquareBase;
extern void enterArea();
extern unsigned char girls_alive[8];

static SCB_REHV_PAL bgSprite = {
    BPP_4 | TYPE_BACKGROUND,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    0, 0,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

static SCB_REHV_PAL cursorSprite = {
    BPP_4 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    80, 51,
    0x100, 0x100,
    {0x01,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

extern unsigned char nrof_enemies();
extern void heal_player(unsigned char love);
extern void delay_enemies(unsigned char i, unsigned char delay);
extern void delay_girl(unsigned char delay);

unsigned char scanloop() {
    int menuindex = 0;
    unsigned int now;
    unsigned char keeprunning = 1;
    unsigned char ret = 0;
    tgi_stdclear();
    tgi_setstdcolor(STD_GREY);
    tgi_outtextxy(2, 2 + off, "BioScan");
    while (keeprunning) {
        unsigned char joy;
        unsigned char i, enemies;
        tgi_setcolor(0);
        tgi_circle(cursorSprite.hpos, cursorSprite.vpos, 5);
        joy = joy_read(JOY_1);
        if (JOY_BTN_UP(joy)) {
            if (cursorSprite.vpos > 0) cursorSprite.vpos -= 1;
        }
        if (JOY_BTN_DOWN(joy)) {
            if (cursorSprite.vpos < 100) cursorSprite.vpos += 1;
        }
        if (JOY_BTN_LEFT(joy)) {
            if (cursorSprite.hpos > 0) cursorSprite.hpos -= 1;
        }
        if (JOY_BTN_RIGHT(joy)) {
            if (cursorSprite.hpos < 159) cursorSprite.hpos += 1;
        }
        if (JOY_BTN_FIRE(joy)) {
            keeprunning = 0;
            ret = 0;
            enterArea();
        }
        if (JOY_BTN_FIRE2(joy)) {
            keeprunning = 0;
            ret = 1;
        }
        enemies = nrof_enemies(); 
        srand(lastIndex);
        tgi_setstdcolor(STD_RED);
        for (i = 1; i <= enemies; i++) {
            unsigned char x, y, d;
            char tmp[8];
            x = (rand() % 156) + 2;
            y = (rand() % 88) + 10;
            d = (abs(x - cursorSprite.hpos)/4 + abs(y - cursorSprite.vpos)/4) / 2;
            tgi_bar(x, y, x+2, y+2);
            //utoa(d, tmp, 10);
            //tgi_outtextxy(x, y, tmp);
            delay_enemies(i-1, d);
        }
        if ((lastSquareBase > 1) && (girls_alive[lastSquareBase])) {
            unsigned char x, y, d;

            tgi_setstdcolor(STD_GREEN);
            x = (rand() % 156) + 2;
            y = (rand() % 88) + 10;
            d = (abs(x - cursorSprite.hpos)/4 + abs(y - cursorSprite.vpos)/4) / 2;
            tgi_bar(x, y, x+3, y+3);
            delay_girl(d);
        }
        tgi_setstdcolor(STD_LIGHTGREY);
        tgi_circle(cursorSprite.hpos, cursorSprite.vpos, 5);
        heal_player(0);
    }
    while (joy_read(JOY_1))
        ;
    return ret;
}
