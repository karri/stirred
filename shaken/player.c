/*
 * Player data
 */
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <stdlib.h>
#include "../resident/tgi_setstdcolor.h"

extern unsigned char enemies_visible();

int player_energy = 1500;
int player_maxenergy = 1500;
int gas_left = 0;
unsigned char nightsight = 0;

int gun_hit = 3;
int gas_hit = 2;

void shoot_player() {
#ifndef TESTING
    if (enemies_visible() > player_energy) {
        player_energy = 0;
    } else {
        player_energy -= enemies_visible();
    }
#endif
}

static unsigned char healtimer = 0;
extern void heal_girl();

void heal_player(unsigned char love) {
    if (love) {
        heal_girl();
    }
    healtimer++;
    if (healtimer > 10) {
        healtimer = 0;
        if (player_energy < player_maxenergy) {
            if (love) {
                player_energy += player_maxenergy / 64;
            } else {
                player_energy += player_maxenergy / 1024;
            }
            if (player_energy > player_maxenergy) {
                player_energy = player_maxenergy;
            }
        }
    }
}

void heal_martini() {
    if (player_energy < player_maxenergy) {
        player_energy += player_maxenergy / 8;
        if (player_energy > player_maxenergy) {
            player_energy = player_maxenergy;
        }
    }
}

void draw_player() {
    int step;
    step = player_maxenergy / 40;
    tgi_setstdcolor(BGCOLOR);
    tgi_bar(2, 98, 2 + 40, 99);
    tgi_setstdcolor(STD_RED);
    tgi_bar(2, 98, 2 + player_energy / step, 99);
}

