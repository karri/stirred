/*
 * countdown
 */
#include <lynx.h>
#include <tgi.h>
#include <stdlib.h>
#include "../resident/tgi_setstdcolor.h"

#define off 0

unsigned int __fastcall__ getscore(void);

int nighttime;

int countdown(unsigned char middle) {
    int maxtime;
    int now;
    int lefttime;
    unsigned char minutes, seconds;
    char buf[6];

    maxtime = 10*60;
    nighttime = maxtime / 2;
    now = getscore();
    if (now > maxtime) {
        return 1;
    }
    lefttime = maxtime - now;
    minutes = lefttime / 60;
    seconds = lefttime - (60 * minutes);
    if (minutes > 9) {
        utoa(minutes, buf, 10);
    } else {
        buf[0] = '0';
        utoa(minutes, buf+1, 10);
    }
    buf[2]=':';
    if (seconds > 9) {
        utoa(seconds, buf+3, 10);
    } else {
        buf[3] = '0';
        utoa(seconds, buf+4, 10);
    }
    buf[5]=0;
    if (middle) {
        tgi_setstdcolor(STD_RED);
        tgi_outtextxy(60, 48 + off, buf);
    } else {
        tgi_setstdcolor(BGCOLOR);
        tgi_bar(2, 2, 5*8+2, 12);
        tgi_setstdcolor(STD_RED);
        tgi_outtextxy(2, 2+off, buf);
    }
    return 0;
}
