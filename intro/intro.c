#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <stdlib.h>
#include <6502.h>
#include "../resident/tgi_setstdcolor.h"

extern unsigned int __fastcall__ getscore(void);


static int hpos = 80;
static int vpos = 51;

#define off 0

int intro() 
{
    int i, d, e;
    int now = getscore();

    d = 0;
    e = 0;
    tgi_stdclear();
    tgi_setstdcolor(STD_LIGHTGREEN);
    tgi_outtextxy(0, 0 + off,  "Shaken, not stirred");
    do {
        tgi_setstdcolor(BGCOLOR);
        tgi_circle(hpos, vpos, 10);
        tgi_setstdcolor(STD_YELLOW);
        if (d) {
            hpos += (rand() & 3);
            if (hpos > 140) {
                d = 0;
            }
        } else {
            hpos -= 1;
            if (hpos < 20) {
                d = 1;
                vpos -= (rand() & 3);
            }
        }
        if (e) {
            vpos += (rand() & 3);
            if (vpos > 80) {
                e = 0;
            }
        } else {
            vpos -= (rand() & 3);
            if (vpos < 20) {
                e = 1;
            }
        }
        tgi_circle(hpos, vpos, 10);
    } while (getscore() < now + 6);
    now = getscore();
    do {
        tgi_setstdcolor(BGCOLOR);
        tgi_circle(hpos, vpos, 10);
        tgi_setstdcolor(STD_YELLOW);
        if (d) {
            hpos += (rand() & 3);
        } else {
            hpos -= 1;
            if (hpos < 20) {
                d = 1;
                vpos -= (rand() & 3);
            }
        }
        if (e) {
            vpos += (rand() & 3);
            if (vpos > 80) {
                e = 0;
            }
        } else {
            vpos -= (rand() & 3);
            if (vpos < 20) {
                e = 1;
            }
        }
        tgi_circle(hpos, vpos, 10);
    } while (getscore() < now + 4);
    now = getscore();
    tgi_setstdcolor(STD_LIGHTGREEN);
    tgi_outtextxy(0, 0 + off,  "Shaken, not stirred");
    tgi_setstdcolor(STD_LIGHTGREEN);
    tgi_outtextxy(0, 10 + off, "  by Kaksonen,");
    tgi_outtextxy(0, 20 + off, "  Karri Kaksonen,");
    tgi_outtextxy(0, 40 + off,  "Find and disarm the");
    tgi_outtextxy(0, 50 + off,  "ticking bomb to");
    tgi_outtextxy(0, 60 + off,  "preserve World");
    tgi_outtextxy(0, 70 + off,  "peace");
    do {
        d = joy_read(JOY_1);
        e = rand();
        if (d) {
            tgi_outtextxy(0, 90 + off,  "You are dismissed.");
        }
    } while (d == 0);
    while (joy_read(JOY_1))
        ;
} 

