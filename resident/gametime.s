	.export		_interruptOccurred
	.interruptor	_gametime
	.export		_playtime
	.export		_paused
	.export		_getscore
	.export		_zerotime

.segment	"DATA"

TIMER2_INTERRUPT = $04
VBL_INTERRUPT = TIMER2_INTERRUPT
INTSET      = $FD81

_interruptOccurred:
	.byte	$00
_playtime:
	.byte	$00
	.byte	$00
	.byte	$00
_paused:
	.byte	$00

; ---------------------------------------------------------------
; void __near__ getscore (void)
; ---------------------------------------------------------------

.segment	"CODE"

.proc	_getscore: near
        lda	_playtime+1
	ldx	_playtime+2
	rts

.endproc

.proc	_zerotime: near
	lda	#0
        sta	_playtime+1
	sta	_playtime+2
	rts

.endproc


.proc	_gametime: near

.segment	"CODE"

	lda	INTSET
	and	#VBL_INTERRUPT
	beq	@goon
	lda	_paused
	bne	@goon
	inc	_interruptOccurred
	lda	_playtime
	ina
	sta	_playtime
	cmp	#75
	bne	@goon
	stz	_playtime
	inc	_playtime+1
	bne	@goon
	inc	_playtime+2
@goon:
	clc
	rts

.endproc

