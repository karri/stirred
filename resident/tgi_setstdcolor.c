/*
 * Header file standard colors
 */
#include <tgi.h>

enum tgicolors {
   STD_BLACK,
   STD_RED,
   STD_PINK,
   STD_LIGHTGREY,
   STD_GREY,
   STD_DARKGREY,
   STD_BROWN,
   STD_PEACH,
   STD_YELLOW,
   STD_LIGHTGREEN,
   STD_GREEN,
   STD_DARKBROWN,
   STD_VIOLET,
   STD_BLUE,
   STD_LIGHTBLUE,
   STD_WHITE
} tgicolors;

//#define FULLCOLOR
//#define FOURCOLOR
//#define BLACK_AND_WHITE
#define WHITE_AND_BLACK
//#define GREEN_MONOCHROME

#ifdef FULLCOLOR
unsigned char BGCOLOR  = STD_BLACK;
static unsigned char pen_available[] = {
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
};
#endif

#ifdef FOURCOLOR
unsigned char BGCOLOR  = STD_BLACK;
static unsigned char pen_available[] = {
    1,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0
};
#endif

#ifdef BLACK_AND_WHITE
unsigned char BGCOLOR  = STD_BLACK;
static unsigned char pen_available[] = {
    1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1
};
#endif

#ifdef WHITE_AND_BLACK
unsigned char BGCOLOR  = STD_WHITE;
static unsigned char pen_available[] = {
    0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1
};
#endif

#ifdef GREEN_MONOCHROME
unsigned char BGCOLOR  = STD_GREEN;
static unsigned char pen_available[] = {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0
};
#endif

static unsigned char pen_index[] = {
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
};

void tgi_setstdcolor(enum tgicolors desired_color)
{
    enum tgicolors color = desired_color;
    unsigned char up = 1;
    unsigned char offset = 1;
    if (pen_available[color] == 0) {
        while ((pen_available[color] == 0) || (color == BGCOLOR)) {
            if (up) {
                if (desired_color + offset <= STD_WHITE) {
                    color = desired_color + offset;
                }
                up = 0;
            } else {
                if (desired_color >= STD_BLACK + offset) {
                    color = desired_color - offset;
                }
                up = 1;
                offset++;
            }
        }
    }
    tgi_setcolor(pen_index[color]);
}

void tgi_stdclear() {
    tgi_clear();
    tgi_setstdcolor(BGCOLOR);
    tgi_bar(0, 0, 159, 101);
}

