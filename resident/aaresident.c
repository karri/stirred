/*
 * The code and data defined here is placed in resident RAM
 */
#include <lynx.h>
#include <tgi.h>
#include <joystick.h>
#include <6502.h>
#include <stdlib.h>
#include "tgi_setstdcolor.h"

extern void zerotime();

unsigned char gamecolor(unsigned char desiredcolor) {
#ifdef NOTDEFINED
    switch (desiredcolor) {
    case 0: // Bg color
        return 0;
    case 1: // Cursor and text color
#ifdef TGI_COLOR_GREY
        return 7;
#else
        return 1;
#endif
    case 2: // Red color
        if (tgi_getcolorcount() == 2) {
            return 1;
        }
#ifdef TGI_COLOR_RED
        return 15;
#else
        return 2;
#endif
    case 3: // Enemy color
        if (tgi_getcolorcount() == 2) {
            return 1;
        }
#ifdef TGI_COLOR_LIGHTGREY
        return TGI_COLOR_LIGHTGREY;
#else
        return 3;
#endif
    default:
        return 0;
    }
#else
    if (desiredcolor)
        return 7;
    else
        return 0;
#endif
}

static SCB_REHV_PAL heartSprite = {
    BPP_1 | TYPE_NORMAL,
    PACKED | REHV,
    NO_COLLIDE,
    0,
    0,
    80, 10,
    0x100, 0x100,
    {0x07,0x23,0x45,0x67,0x89,0xab,0xcd,0xef}
};

enum operatingmodes {
    MAP,
    SCAN,
    FIGHT,
    DISARM
};

extern unsigned char paused;

extern void create_world();

static void initlynx() {
    tgi_install(&tgi_static_stddrv);
    tgi_init();
    joy_install(&joy_static_stddrv);
    CLI();
};
extern void intro();
extern void maploop();
extern unsigned char scanloop();
extern unsigned char fightloop();
extern int disarmloop();
extern void setdefpal();
extern unsigned int __fastcall__ getscore(void);

enum ending {
    SUCCESS,
    KILLED,
    TIMEOUT,
    EXPLODED
} ending;

void endloop(enum ending endgame) {
    int now = getscore();
    do {
    while(tgi_busy())
        ;
    setdefpal();
    tgi_stdclear();
    tgi_setstdcolor(STD_RED);
    switch (endgame) {
    case SUCCESS:
        tgi_outtextxy(10, 40, "   Ooh James");
        tgi_outtextxy(10, 50, "that was awesome");
        if (getscore() > now + 2) {
            heartSprite.vpos=10;
            heartSprite.hpos=110;
            heartSprite.penpal[0] = COLOR_PINK;
            tgi_sprite(&heartSprite);
            
        }
        if (getscore() > now + 3) {
            heartSprite.penpal[0] = COLOR_RED;
            heartSprite.vpos=15;
            heartSprite.hpos=125;
            tgi_sprite(&heartSprite);
        }
        break;
    case KILLED:
        tgi_outtextxy(2, 40, "Rest in peace");
        break;
    case TIMEOUT:
    case EXPLODED:
        tgi_outtextxy(40, 40, "Big Bada Boom");
        break;
    }
    tgi_updatedisplay();
    } while (getscore() < now + 6);
    while (1)
        ;
}

void main(void)
{
    enum operatingmodes menumode = MAP;
    unsigned char menuindex = 0;
    unsigned char keeprunning = 1;
    enum ending endgame;

    initlynx();
    intro();
    paused = 0;
    create_world();
    zerotime();
    while (keeprunning) {
        switch (menumode) {
        case MAP:
            maploop();
            menumode = SCAN;
            break;
        case SCAN:
            if (scanloop() == 0) {
                menumode = FIGHT;
            } else {
                menumode = MAP;
            }
            break;
        case FIGHT:
            switch (fightloop()) {
            case 0:
                // success
                menumode = MAP;
                break;
            case 1:
                endgame = KILLED;
                keeprunning = 0;
                break;
            case 2:
                menumode = DISARM;
                break;
            case 3:
                endgame = TIMEOUT;
                keeprunning = 0;
                break;
            default:
                break;
            }
            break;
        case DISARM:
            if (disarmloop()) {
                endgame = EXPLODED;
            } else {
                endgame = SUCCESS;
            }
            keeprunning = 0;
            break;
        default:
            menumode = FIGHT;
            break;
        }
    }
    endloop(endgame);
}

