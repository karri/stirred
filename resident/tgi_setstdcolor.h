/*
 * Header file standard colors
 */
#include <tgi.h>

typedef enum tgicolors {
   STD_BLACK,
   STD_RED,
   STD_PINK,
   STD_LIGHTGREY,
   STD_GREY,
   STD_DARKGREY,
   STD_BROWN,
   STD_PEACH,
   STD_YELLOW,
   STD_LIGHTGREEN,
   STD_GREEN,
   STD_DARKBROWN,
   STD_VIOLET,
   STD_BLUE,
   STD_LIGHTBLUE,
   STD_WHITE
} tgicolors;

extern void tgi_setstdcolor(enum tgicolors desired_color);
extern void tgi_stdclear();
extern unsigned char BGCOLOR;
